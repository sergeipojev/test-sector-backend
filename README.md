#### Test project of Sergei Pojev, backend on Spring Boot, java, gradle.

After run, application will be available on port 8080, http://localhost:8080

#### Database

For this project, Postgres database was used. I used my own database.
DB connection properties can be chaned in
	
		/src/main/resources/application.properties

Database dump is in the root folder under
	
		/database_dump

NB! Please note, that there is no process which would add sectors to DB. All sectors should be inserted manually to Sector table in DB.