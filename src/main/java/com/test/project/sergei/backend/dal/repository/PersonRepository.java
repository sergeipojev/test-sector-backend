package com.test.project.sergei.backend.dal.repository;

import com.test.project.sergei.backend.dal.domain.Person;

public interface PersonRepository extends BaseRepository<Person, Long> {
}
