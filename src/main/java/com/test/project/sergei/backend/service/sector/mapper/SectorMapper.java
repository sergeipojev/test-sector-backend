package com.test.project.sergei.backend.service.sector.mapper;

import com.test.project.sergei.backend.dal.domain.Sector;
import com.test.project.sergei.backend.dal.dto.SectorDTO;
import lombok.experimental.UtilityClass;

@UtilityClass
public class SectorMapper {

    public static SectorDTO mapFromDomain(Sector sector) {
        SectorDTO sectorDTO = new SectorDTO();
        sectorDTO.setName(sector.getName());
        sectorDTO.setId(sector.getId());
        sectorDTO.setCode(sector.getCode());
        sectorDTO.setParentId(sector.getParentId());

        return sectorDTO;
    }

    public static Sector mapFromDTO(SectorDTO sectorDTO) {
        Sector sector = new Sector();

        sector.setId(sectorDTO.getId());
        sector.setName(sectorDTO.getName());
        sector.setCode(sectorDTO.getCode());
        sector.setParentId(sectorDTO.getParentId());

        return sector;
    }
}
