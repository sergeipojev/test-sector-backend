package com.test.project.sergei.backend.service.sector;

import com.test.project.sergei.backend.dal.domain.Sector;
import com.test.project.sergei.backend.dal.dto.SectorDTO;
import com.test.project.sergei.backend.dal.repository.SectorRepository;
import com.test.project.sergei.backend.service.sector.mapper.SectorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    /**
     * Getting all sectors available in database,
     * @return Structured list.
     */
    public List<SectorDTO> getAllSectors() {

        List<Sector> all = sectorRepository.findAll(Sort.by(Sort.Direction.DESC, "parentId"));


        List<SectorDTO> dtoList = new ArrayList<>();
        this.processList(dtoList, all.stream().filter(e -> e.getParentId() == null).collect(Collectors.toList()), 0);

        return dtoList;
    }

    private void processList(List<SectorDTO> listOfSectors, List<Sector> list, int currentLevel) {
        if (list.size() == 0) {
            return;
        }

        list.forEach(e -> {
            SectorDTO sectorDTO = SectorMapper.mapFromDomain(e);
            sectorDTO.setIndentinationCount(currentLevel);
            listOfSectors.add(sectorDTO);
            processList(listOfSectors, new ArrayList<>(e.getChildSectors()), currentLevel+1);
        });
    }

}
