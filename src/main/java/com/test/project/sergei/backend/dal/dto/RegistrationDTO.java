package com.test.project.sergei.backend.dal.dto;

import lombok.Data;

import java.util.List;

@Data
public class RegistrationDTO {
    private Long id;
    private String name;
    private boolean termsConditions;
    private List<SectorDTO> sectors;
}
