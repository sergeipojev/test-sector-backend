package com.test.project.sergei.backend.controller;

import com.test.project.sergei.backend.dal.dto.SectorDTO;
import com.test.project.sergei.backend.service.sector.SectorService;
import com.test.project.sergei.backend.utility.constants.Endpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(Endpoint.SECTOR)
public class SectorController {

    @Autowired
    private SectorService sectorService;

    /**
     * Get structured list of all sectors available.
     * @return List of sectors.
     */
    @GetMapping
    public List<SectorDTO> allSectors() {
        return this.sectorService.getAllSectors();
    }
}
