package com.test.project.sergei.backend.utility.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Endpoint {

    // Heartbeat
    public static final String HEARTBEAT = "/api/heartbeat";

    // Sector
    public static final String SECTOR = "/api/sector";

    // Registration
    public static final String REGISTRATION = "/api/registration";

}
