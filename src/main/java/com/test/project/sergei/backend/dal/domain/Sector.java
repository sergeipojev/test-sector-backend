package com.test.project.sergei.backend.dal.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Generated;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "sector")
public class Sector {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name")
    @JsonProperty("name")
    @Length(max = 48)
    private String name;

    @NotNull
    @Column(name = "code")
    @JsonProperty("code")
    private int code;

    @Column(name = "parentid")
    @JsonProperty("parentid")
    private Long parentId;

    @OneToMany(mappedBy = "parentId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Sector> childSectors = new LinkedHashSet<>();
}
