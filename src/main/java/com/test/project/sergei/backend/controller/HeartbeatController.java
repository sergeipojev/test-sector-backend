package com.test.project.sergei.backend.controller;

import com.test.project.sergei.backend.utility.constants.Endpoint;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoint.HEARTBEAT)
public class HeartbeatController {

    /**
     * Heartbeat.
     * @return
     */
    @GetMapping
    public String heartbeat() {
        return "Heartbeat OK";
    }
}
