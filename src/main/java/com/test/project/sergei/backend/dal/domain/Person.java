package com.test.project.sergei.backend.dal.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name")
    @JsonProperty("name")
    @Length(max = 36)
    private String name;

    @NotNull
    @Column(name = "termsandconditions")
    @JsonProperty("termsandconditions")
    private boolean termsAndConditionsAccepted;

    @NotNull
    @Column(name = "registeredon")
    @JsonProperty("registeredon")
    private LocalDateTime registered;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "sectorsinvolved",
            joinColumns = @JoinColumn(name = "personid"),
            inverseJoinColumns = @JoinColumn(name = "sectorid"))
    private Set<Sector> sectors = new LinkedHashSet<>();

    @PrePersist
    void preInsert() {
        if (this.registered == null) this.registered = LocalDateTime.now();
    }

    @PreUpdate
    void preUpdate()  {
        if (this.registered == null) this.registered = LocalDateTime.now();
    }
}
