package com.test.project.sergei.backend.service.registraiton.mapper;

import com.test.project.sergei.backend.dal.domain.Person;
import com.test.project.sergei.backend.dal.dto.RegistrationDTO;
import com.test.project.sergei.backend.service.sector.mapper.SectorMapper;
import lombok.experimental.UtilityClass;

import java.util.stream.Collectors;

@UtilityClass
public class RegistrationMapper {
    public static RegistrationDTO mapFromDomain(Person person) {
        RegistrationDTO registrationDTO = new RegistrationDTO();

        registrationDTO.setId(person.getId());
        registrationDTO.setName(person.getName());
        registrationDTO.setTermsConditions(person.isTermsAndConditionsAccepted());
        registrationDTO.setSectors(person.getSectors().stream()
                .map(SectorMapper::mapFromDomain).collect(Collectors.toList()));

        return registrationDTO;
    }

    public static Person mapFromDTO(RegistrationDTO registration) {
        Person person = new Person();

        person.setId(registration.getId());
        person.setName(registration.getName());
        person.setTermsAndConditionsAccepted(registration.isTermsConditions());
        person.setSectors(registration.getSectors().stream()
                .map(SectorMapper::mapFromDTO).collect(Collectors.toSet()));

        return person;
    }


}
