package com.test.project.sergei.backend.dal.repository;

import com.test.project.sergei.backend.dal.domain.Sector;

public interface SectorRepository extends BaseRepository<Sector, Long> {
}
