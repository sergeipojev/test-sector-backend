package com.test.project.sergei.backend.dal.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SectorDTO {

    private Long id;
    private String name;
    private Long parentId;
    private int code;
    private int indentinationCount;

    private List<SectorDTO> childSectors = new ArrayList<>();
}
