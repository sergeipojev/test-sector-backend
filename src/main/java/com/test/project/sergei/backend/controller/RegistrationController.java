package com.test.project.sergei.backend.controller;

import com.test.project.sergei.backend.dal.dto.RegistrationDTO;
import com.test.project.sergei.backend.service.registraiton.RegistrationSevice;
import com.test.project.sergei.backend.utility.constants.Endpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;

@RestController
@RequestMapping(Endpoint.REGISTRATION)
public class RegistrationController {

    @Autowired
    private RegistrationSevice registrationSevice;

    /**
     * Get registration by specified ID.
     * @param id ID of the registration.
     * @return Registration.
     * @throws ValidationException Current registration was not found.
     */
    @GetMapping(path = "/{id}")
    public RegistrationDTO getRegistration(@PathVariable("id") Long id) throws ValidationException {
        return this.registrationSevice.getRegistration(id);
    }

    /**
     * Save new registration to system.
     * @param registrationDTO Registration object.
     * @return Saved registration.
     * @throws ValidationException Data validation problems.
     */
    @PostMapping
    public RegistrationDTO saveRegistration(@RequestBody RegistrationDTO registrationDTO) throws ValidationException {
        return this.registrationSevice.saveOrUpdateRegistration(registrationDTO);
    }

    /**
     * Update existing registration.
     * @param registrationDTO Registration object.
     * @return Saved registration.
     * @throws ValidationException Data validation problems.
     */
    @PutMapping
    public RegistrationDTO updateRegistration(@RequestBody RegistrationDTO registrationDTO) throws ValidationException {
        return this.registrationSevice.saveOrUpdateRegistration(registrationDTO);
    }


}
