package com.test.project.sergei.backend.service.registraiton;

import com.test.project.sergei.backend.dal.domain.Person;
import com.test.project.sergei.backend.dal.dto.RegistrationDTO;
import com.test.project.sergei.backend.dal.repository.PersonRepository;
import com.test.project.sergei.backend.service.registraiton.mapper.RegistrationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.ValidationException;
import java.util.Optional;

@Service
public class RegistrationSevice {

    @Autowired
    private PersonRepository personRepository;

    public RegistrationDTO getRegistration(Long id) throws ValidationException {
        Optional<Person> person = this.personRepository.findById(id);

        if (!person.isPresent()) {
            throw new ValidationException("Registration with id " + id + " not found in database");
        }

        return RegistrationMapper.mapFromDomain(person.get());
    }

    public RegistrationDTO saveOrUpdateRegistration(RegistrationDTO registration) throws ValidationException {
        // Validating input
        this.validateInput(registration);

        Person person = this.personRepository.save(RegistrationMapper.mapFromDTO(registration));
        return RegistrationMapper.mapFromDomain(person);
    }

    public RegistrationDTO updateRegistration(RegistrationDTO registration) throws ValidationException {
        this.validateInput(registration);

        Person person = this.personRepository.save(RegistrationMapper.mapFromDTO(registration));
        return RegistrationMapper.mapFromDomain(person);
    }

    private void validateInput(RegistrationDTO registration) throws ValidationException {
        if (registration.getName() == null || registration.getName().isEmpty()) {
            throw new ValidationException("Name field cannot be empty");
        }

        if (registration.getSectors() == null || registration.getSectors().size() == 0) {
            throw new ValidationException("At least one sector should be selected");
        }

        if (!registration.isTermsConditions()) {
            throw new ValidationException("You should accept terms and conditions");
        }
    }
}
