package com.test.project.sergei.backend;

import com.test.project.sergei.backend.dal.domain.Sector;
import com.test.project.sergei.backend.dal.dto.RegistrationDTO;
import com.test.project.sergei.backend.dal.dto.SectorDTO;
import com.test.project.sergei.backend.service.registraiton.RegistrationSevice;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.ValidationException;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RegistrationTest {

    @Autowired
    private RegistrationSevice registrationSevice;

    @Test(expected = ValidationException.class)
    public void testValidationNoName() throws ValidationException {
        RegistrationDTO registrationDTO = new RegistrationDTO();
        registrationDTO.setTermsConditions(true);
        registrationDTO.setSectors(generateSectorList());

        this.registrationSevice.saveOrUpdateRegistration(registrationDTO);
    }

    @Test(expected = ValidationException.class)
    public void testNoSectorsSelected() throws ValidationException {
        RegistrationDTO registrationDTO = new RegistrationDTO();
        registrationDTO.setTermsConditions(true);
        registrationDTO.setName("Test Name");

        this.registrationSevice.saveOrUpdateRegistration(registrationDTO);
    }

    @Test(expected = ValidationException.class)
    public void termsAndConditionsNotAccepted() throws ValidationException {
        RegistrationDTO registrationDTO = new RegistrationDTO();
        registrationDTO.setTermsConditions(false);
        registrationDTO.setName("Test Name");
        registrationDTO.setSectors(generateSectorList());


        this.registrationSevice.saveOrUpdateRegistration(registrationDTO);
    }

    private List<SectorDTO> generateSectorList() {
        List<SectorDTO> sectors = new ArrayList<>();

        SectorDTO s1 = new SectorDTO();
        s1.setId(3L);
        s1.setName("Construction materials");
        s1.setCode(2);
        sectors.add(s1);

        SectorDTO s2 = new SectorDTO();
        s2.setId(6L);
        s2.setName("Beverages");
        s2.setCode(5);
        sectors.add(s2);

        return sectors;
    }
}
