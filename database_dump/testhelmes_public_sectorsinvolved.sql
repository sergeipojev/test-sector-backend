create table sectorsinvolved
(
  id       serial  not null
    constraint sectorsinvolved_pk
      primary key,
  personid integer not null
    constraint sectorsinvolved_person_id_fk
      references person,
  sectorid integer not null
    constraint sectorsinvolved_sector_id_fk
      references sector
);

alter table sectorsinvolved
  owner to postgres;

create unique index sectorsinvolved_id_uindex
  on sectorsinvolved (id);

INSERT INTO public.sectorsinvolved (id, personid, sectorid) VALUES (1, 1, 7);
INSERT INTO public.sectorsinvolved (id, personid, sectorid) VALUES (5, 2, 5);
INSERT INTO public.sectorsinvolved (id, personid, sectorid) VALUES (11, 5, 6);
INSERT INTO public.sectorsinvolved (id, personid, sectorid) VALUES (12, 6, 11);
INSERT INTO public.sectorsinvolved (id, personid, sectorid) VALUES (14, 6, 6);