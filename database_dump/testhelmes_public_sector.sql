create table sector
(
  id       serial  not null
    constraint sector_pk
      primary key,
  name     varchar not null,
  parentid integer
    constraint sector_sector_id_fk
      references sector,
  code     integer not null
);

comment on table sector is 'Sectors list';

alter table sector
  owner to postgres;

create unique index sector_id_uindex
  on sector (id);

INSERT INTO public.sector (id, name, parentid, code) VALUES (3, 'Construction materials', 2, 2);
INSERT INTO public.sector (id, name, parentid, code) VALUES (7, 'Other', 5, 6);
INSERT INTO public.sector (id, name, parentid, code) VALUES (2, 'Manufacturing', null, 1);
INSERT INTO public.sector (id, name, parentid, code) VALUES (6, 'Beverages', 5, 5);
INSERT INTO public.sector (id, name, parentid, code) VALUES (5, 'Food and Beverage', 2, 4);
INSERT INTO public.sector (id, name, parentid, code) VALUES (4, 'Electronics and optics', 2, 3);
INSERT INTO public.sector (id, name, parentid, code) VALUES (8, 'Machinery', null, 244);
INSERT INTO public.sector (id, name, parentid, code) VALUES (9, 'Machinery components', 8, 888);
INSERT INTO public.sector (id, name, parentid, code) VALUES (10, 'Machinery equipment/tools', 8, 777);
INSERT INTO public.sector (id, name, parentid, code) VALUES (11, 'Manufacture of machinery', 10, 989);