create table person
(
  id                 serial                not null
    constraint person_pk
      primary key,
  name               varchar               not null,
  termsandconditions boolean default false not null,
  registeredon       timestamp             not null
);

comment on table person is 'Persons table';

alter table person
  owner to postgres;

create unique index person_id_uindex
  on person (id);

INSERT INTO public.person (id, name, termsandconditions, registeredon) VALUES (1, 'tedst', true, '2019-06-14 15:59:39.793000');
INSERT INTO public.person (id, name, termsandconditions, registeredon) VALUES (2, 'Sergei Pojev', true, '2019-06-14 16:16:41.460000');
INSERT INTO public.person (id, name, termsandconditions, registeredon) VALUES (5, 'Test 3', true, '2019-06-14 16:47:02.163000');
INSERT INTO public.person (id, name, termsandconditions, registeredon) VALUES (6, 'Sergeo Tar', true, '2019-06-14 17:53:19.265000');